# Splunk in Higher Education
An effort in creating a collaborative enironment for Splunk administrators in higher education.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
## Goals

1. Develop Best Practices
2. Reduce Duplication of Effort
3. Share Splunk Expertise
4. Share Various Technological Expertise

### Develop Best Practices
While Splunk does provide some best practices, they are not always easy to find and tend to be a bit anemic. By leveraging our community, I hope to develop, test, refine, and mature best practices for the whole Splunk community.

### Reduce Duplication of Effort
Reduce duplication of effort through two main initiateives: jointly developing and maturing Splunk content leveraging our shared expertise and by easily making the content available formated in a way that can actually be used -- that is to say there will not be one "master" TA that needs to be ripped apart but files will be stored in ways to allow them to be used directly[^1]
[^1] While you can directly use a TA, that does not mean you should.  Please see git best practices. *TODO* make a link

## Why higher education?
Because I work in higher education and we have a history of collaboration. It is my personal hope that we can spread into other verticals, to public collaboration, etc. dependent on the project, but there had to be an initial scope and this is the line I drew.


# Why?
One of my biggest best peeves is duplication of effort. I understand Splunk's desire to have things in a state where a person with a single server instance to be able to easily hit the install button and see the wonder that is Splunk, but the reality is most of their customers are in a distributed environment and it  causes us to constantly rip apart TA's
# Why git?

# Why Gitlab?
The short answer is gitlab allows free private repo's and doesn't limit the number of collaborators so it was the easiest option to try.

I think many people would prefer something hosted by an edu; unfortunately when this came up on the mailing list no one stepped forward as having a solution.  A couple of schools could setup something, but that meant one more tool to keep up with, potentially a significant number of accounts to monitor, and I wanted to minimize the "cost" associate with this effort as I really wanted to increase value to participating schools.

While this does potentially open the door to larger exposure, I would not expect the information to be sensitive in nature.  Apps that are hosted here are 

----

Forked from @virtuacreative

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
